# Changelog

## v1.0.1 - (2017-06-20)
### Changed
  * README.md instruction updates

## v1.0.0 - (2019‑05‑14)
- Initial Release