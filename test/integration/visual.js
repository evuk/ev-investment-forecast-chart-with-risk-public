const puppeteer = require('puppeteer');
const expect = require('chai').expect;
const { startServer } = require('polyserve');
const path = require('path');
const fs = require('fs');
const PNG = require('pngjs').PNG;
const pixelmatch = require('pixelmatch');

const currentDir = `${process.cwd()}/test/integration/screenshots-current`;
const baselineDir = `${process.cwd()}/test/integration/screenshots-baseline`;

describe('👀 page screenshots are correct at width 1055', function () {
  let polyserve, browser, page;

  before(async function () {
    polyserve = await startServer({ port: 4444, root: path.join(__dirname, '../..'), moduleResolution: 'node' });

    // Create the test directory if needed.
    if (!fs.existsSync(currentDir)) {
      fs.mkdirSync(currentDir);
    }
  });

  after((done) => polyserve.close(done));

  beforeEach(async function () {
    browser = await puppeteer.launch({defaultViewport: {width: 1055, height: 700}});
    page = await browser.newPage();
  });

  afterEach(() => browser.close());

  describe('screen', function () {
    it('demo', async function () {
      return takeAndCompareScreenshot(page, 1);
    });
  });
});

describe('👀 page screenshots are correct at width 800', function () {
  let polyserve, browser, page;

  before(async function () {
    polyserve = await startServer({ port: 4444, root: path.join(__dirname, '../..'), moduleResolution: 'node' });

    // Create the test directory if needed.
    if (!fs.existsSync(currentDir)) {
      fs.mkdirSync(currentDir);
    }
  });

  after((done) => polyserve.close(done));

  beforeEach(async function () {
    browser = await puppeteer.launch({defaultViewport: {width: 800, height: 970}});
    page = await browser.newPage();
  });

  afterEach(() => browser.close());

  describe('screen', function () {
    it('demo', async function () {
      return takeAndCompareScreenshot(page, 2);
    });
  });
});

describe('👀 page screenshots are correct at width 768', function () {
  let polyserve, browser, page;

  before(async function () {
    polyserve = await startServer({ port: 4444, root: path.join(__dirname, '../..'), moduleResolution: 'node' });

    // Create the test directory if needed.
    if (!fs.existsSync(currentDir)) {
      fs.mkdirSync(currentDir);
    }
  });

  after((done) => polyserve.close(done));

  beforeEach(async function () {
    browser = await puppeteer.launch({defaultViewport: {width: 768, height: 1100}});
    page = await browser.newPage();
  });

  afterEach(() => browser.close());

  describe('screen', function () {
    it('demo', async function () {
      return takeAndCompareScreenshot(page, 3);
    });
  });
});

async function takeAndCompareScreenshot(page, indexNo) {
  // If you didn't specify a file, use the name of the route.
  await page.goto('http://127.0.0.1:4444/components/ev-investment-forecast-chart-with-risk/demo/screenshot_testing_index.html');
  await page.waitFor(5000);
  await page.screenshot({path: `${currentDir}/index${indexNo}.png`});
  return compareScreenshots(indexNo);
}

function compareScreenshots(indexNo) {
  return new Promise((resolve, reject) => {
    // Note: for debugging, you can dump the screenshotted img as base64.
    // fs.createReadStream(`${currentDir}/index.png`, { encoding: 'base64' })
    //   .on('data', function (data) {
    //     console.log('got data', data)
    //   })
    //   .on('end', function () {
    //     console.log('\n\n')
    //   });
    const img1 = fs.createReadStream(`${currentDir}/index${indexNo}.png`).pipe(new PNG()).on('parsed', doneReading);
    const img2 = fs.createReadStream(`${baselineDir}/index${indexNo}.png`).pipe(new PNG()).on('parsed', doneReading);

    let filesRead = 0;
    function doneReading() {
      // Wait until both files are read.
      if (++filesRead < 2) return;

      // The files should be the same size.
      expect(img1.width, 'image widths are the same').equal(img2.width);
      expect(img1.height, 'image heights are the same').equal(img2.height);

      // Do the visual diff.
      const diff = new PNG({ width: img1.width, height: img1.height });

      // Skip the bottom/rightmost row of pixels, since it seems to be
      // noise on some machines :/
      const width = img1.width - 1;
      const height = img1.height - 1;

      const numDiffPixels = pixelmatch(img1.data, img2.data, diff.data,
        width, height, { threshold: 0.2 });
      const percentDiff = numDiffPixels / (width * height) * 100;

      const stats = fs.statSync(`${currentDir}/index.png`);
      const fileSizeInBytes = stats.size;
      console.log(`📸 index.png => ${fileSizeInBytes} bytes, ${percentDiff}% different`);

      //diff.pack().pipe(fs.createWriteStream(`${currentDir}/index-diff.png`));
      expect(numDiffPixels, 'number of different pixels').equal(0);
      resolve();
    }
  });
}