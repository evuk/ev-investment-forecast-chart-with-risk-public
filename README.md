# <ev-investment-forecast-chart-with-risk\>

A simple investment forecast widget with a slider to input risk.
This widget projects a stochastic pension forecast based on an initial sum, regular monthly
contributions, the term the money will be invested (the number of years) and the risk group.

To provide a forecast, the lump sum, contribution, term and fund code must be entered and validated
by the widget.

## Browser Support
This component has been tested with the following browsers

* **Google Chorme**
* **Firefox**
* **Microsoft Edge**
* **Safari**

## Connecting to EValue's APIs
---
First you need to sign up to the Portal, which you can do on our website, using just your name and email.

https://api-store.evalueproduction.com/store/site/pages/sign-up.jag

## Subscribing to an API
---
Sign into the EValue Developer Portal and then follow the detailed instructions at [EValue Portal – Getting Started – Subscribing to individual APIs](https://api.ev.uk/getting-started.php#individualApis).


## Generating Access Keys
---
Once you’ve signed up, you need to generate the access key required for authentication. This can be done by following the instructions on the [EValue Developer Portal](https://api.ev.uk/getting-started.php), under the section [‘Generating access keys’](https://api.ev.uk/getting-started.php#generatingKeys).

## Installation
---
Although the ev-investment-forecast-chart-with-risk is a bower package, it is not registered in the bower registry. To install it you will need to tell bower CLI where to look for the package. So intead of running

```sh
$ bower install –-save ev-investment-forecast-chart-with-risk-public
```
you will need to run:

```sh
$ bower install --save https://bitbucket.org/evuk/ev-investment-forecast-chart-with-risk-public
```
Alternatively, you can create a bower.json by hand

```json
{
    "name": "your-application",
    "dependencies": {
		"ev-investment-forecast-chart-with-risk-public": "https://bitbucket.org/evuk/ev-investment-forecast-chart-with-risk-public.git#v1.0.0"
    }
}
```
Then run ```bower install``` in the directory where the bower.json resides.

## Including component in HTML Example
---

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, user-scalable=yes">
    <title>Demo</title>
    <link rel="import" href="bower_components/ev-investment-forecast-chart-with-risk-public/ev-investment-forecast-chart-with-risk.html">
    <script src="bower_components/webcomponentsjs/webcomponents-loader.js" defer></script>
  </head>
  <body>
    <div>
      <h3> Component Demo</h3>
      <ev-investment-forecast-chart-with-risk authentication-token="token-here"></ev-investment-forecast-chart-with-risk>
    </div>
  </body>
</html>
```
To initialize the widget, you must pass the authentication token from your EValue API Portal account to the HTML attribute `authentication-token`. You can pass a sandbox or production token however this might produce different in forecast results.

## Styling
---

The following custom CSS variables are also available for custom styling:

Custom CSS property | Description | Default
------------------------------------------|---------------------------------|----------------------
`--ev-risk-slider-pin-color`                 | Color for the risk slider pin    | `#4285F4`
`--ev-risk-slider-container-color`           | Color for the risk slider container    | `#C3D4F1`
`--ev-risk-slider-active-color`           | Color for the risk slider when active    | `#4285F4`
`--ev-risk-slider-markers-color`           | Color for risk slider markers    | `#4285F4`
`--ev-risk-slider-knob-color`           | Color for the slider knob    | `#4285F4`
`--ev-high-result-color`           | Color for the high percentile returns    | `#87AB1E`
`--ev-mid-result-color`           | Color for the mid percentile returns    | `#87AB1E`
`--ev-low-result-color`           | Color for the low percentile returns    | `#87AB1E`
`--ev-font-common-base`           | CSS Mixin for applying fonts     | `{ font-family: 'Nunito Sans', Arial, Verdana, sans-serif; -webkit-font-smoothing: antialiased; color: var(--ev-text-primary-color); font-size: 16px; }`

Here is a example of how to define some custom CSS variables

```html
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, user-scalable=yes">
    <title>Demo</title>
    <script src="bower_components/webcomponentsjs/webcomponents-loader.js"></script>
    <script src="bower_components/ev-investment-forecast-chart-with-risk-public/ev-investment-forecast-chart-with-risk.html"></script>
    <!-- custom-style element invokes the custom properties polyfill -->
    <script src="bower_components/polymer/lib/elements/custom-style.html"></script>
  </head>

 <!-- ensure that custom props are polyfilled on browsers that don't support them -->
  <custom-style>
    <style>
      ev-investment-forecast-chart-with-risk {
        --ev-high-result-color: red;
        --ev-mid-result-color: green;
        --ev-low-result-color: blue;
      }
    </style>
    ...
  </custom-style>
```

Furthermore there are more ways to override styling through HTML attributes:

Custom HTML Attribute | Description | Default
------------------------------------------|---------------------------|----------------------
`currency`              | Currency symbol used for input fields     | `£`
`high-circle-colour`              | Color of the high percentile node within the chart     | `#D5E2B0`
`mid-circle-colour`              | Color of the mid percentile node within the chart     | `#87AB1E`
`low-circle-colour`              | Color of the low percentile node within the chart     | `#D5E2B0`
`funnel-colour`              | Color of the funnel chart when results are displayed     | `#87AB1E`
`risk-group`              | The default number used for the risk slider (which should only be 5, 7 or 10)    | `5`

Similarly, you can also override these attributes as JavaScript properties by using the camelCase convention.
For example, the attribute `first-name` maps to `firstName`.

```html
  <body>
    <div>
      <ev-investment-forecast-chart-with-risk mid-circle-colour="yellow" id="widget"></ev-investment-forecast-chart-with-risk>
    </div>
  </body>

  <script>
    var widget = document.getElementById('widget');
    widget.riskGroup = 7;
    widget.funnelColour = pink;
  </script>
```

## Polyfills
---

As HTML Web Components use a set of new standards, these are not natively supported by older browsers and require a set of polyfills so that all features work as expected. The polyfills Javascript library contains a ‘loader’ script which will check the user’s browser to see which of the four main specifications that web components are based on are supported. It will then load the polyfills required for the component to display and function correctly.

Here are a list of polyfills you might want to include:

**webcomponents-lite.js** includes all of the polyfills necessary to run on any of the supported browsers. Because all browsers receive all polyfills, there is an extra overhead when using this.

**webcomponents-loader.js** performs client-side feature-detection and loads just the required polyfills. This requires an extra round-trip to the server but saves bandwidth for browsers that support one or more features.

**custom-elements-es5-adapter.js** essentially wraps ES5 compiled Custom Elements to work across browsers. This polyfill must load **before** defining a ES5 Custom Element. This adapter will automatically wrap ES5. This adapter must **NOT** be compiled.

```html
<script src="bower_components/webcomponentsjs/custom-elements-es5-adapter.js"></script>
<script src="bower_components/webcomponentsjs/webcomponents-loader.js"></script>
```

Click [here](https://github.com/webcomponents/webcomponentsjs) to learn more about **webcomponentsloader.js** and the other polyfills listed above.